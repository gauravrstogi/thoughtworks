//
//  Product.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Product: BaseModel {
    var sku:String?
    var name:String?
    var category:Category?
    var itemCount:NSNumber?
    var subCategory:SubCategory?
    var productDescription:String?
    var price:Price!
    var images:[Image] = []
    var isFeatured:Bool = false
    
    override init(dictionary: NSDictionary!) {
        if dictionary.count>0{
            sku = dictionary.stringObjectForKey("sku")
            name = dictionary.stringObjectForKey("name")
            
            itemCount = dictionary.numberObjectForKey("itemCount")
            if let categoryId = CategoryID(rawValue: (dictionary["categoryId"] as! NSNumber).integerValue){
                category = Utility.findCategoryForId(categoryId)
            }
            if let subCategoryId = SubCategoryID(rawValue: (dictionary["sub_categoryId"] as! NSNumber).integerValue){
                subCategory = Utility.findSubCategoryForId(subCategoryId)
            }
            productDescription = dictionary.stringObjectForKey("productDescription")
            if let priceDict = dictionary.dictionaryObjectForKey("price"){
                price = Price(dictionary: priceDict)
            }
            
            if let list = dictionary.objectForKey("images") as? NSArray{
                for object in list{
                    images.append(Image(dictionary: object as! NSDictionary))
                }
            }
            isFeatured = (dictionary["is_featured"] as! NSNumber).boolValue
        }
        super.init(dictionary: dictionary)
    }
    
    //MARK: - NSCoding -
    required convenience init(coder aDecoder: NSCoder) {
        self.init(dictionary: [String:AnyObject]())
        sku = aDecoder.decodeObjectForKey("sku") as? String
        name = aDecoder.decodeObjectForKey("name") as? String
        itemCount = aDecoder.decodeObjectForKey("itemCount") as? NSNumber
        category = aDecoder.decodeObjectForKey("category") as? Category
        subCategory = aDecoder.decodeObjectForKey("subCategory") as? SubCategory
        productDescription = aDecoder.decodeObjectForKey("productDescription") as? String
        price = aDecoder.decodeObjectForKey("price") as! Price
        images = aDecoder.decodeObjectForKey("images") as! [Image]
        isFeatured = aDecoder.decodeObjectForKey("isFeatured") as! Bool
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(sku, forKey: "sku")
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(itemCount, forKey: "itemCount")
        aCoder.encodeObject(category, forKey: "category")
        aCoder.encodeObject(subCategory, forKey: "subCategory")
        aCoder.encodeObject(productDescription, forKey: "productDescription")
        aCoder.encodeObject(price, forKey: "price")
        aCoder.encodeObject(images, forKey: "images")
        aCoder.encodeObject(isFeatured, forKey: "isFeatured")
    }
}
