//
//  ProductImage.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Image: BaseModel {
    var url:NSURL!
    var isDefault:Bool = false
    
    override init(dictionary: NSDictionary!) {
        if dictionary.count>0{
            if let string = dictionary.stringObjectForKey("url"){
                url = NSURL(string: string)
            }
            
            isDefault = dictionary.boolValueForKey("isDefault")
        }
        super.init(dictionary:dictionary)
    }
    
    //MARK: - NSCoding -
    required convenience init(coder aDecoder: NSCoder) {
        self.init(dictionary: [String:AnyObject]())
        url = aDecoder.decodeObjectForKey("url") as! NSURL
        isDefault = aDecoder.decodeObjectForKey("isDefault") as! Bool
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(url, forKey: "url")
        aCoder.encodeObject(isDefault, forKey: "isDefault")
    }
}
