//
//  Price.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Price: BaseModel {
    
    var isDiscountAvailable:Bool = false
    var originalPrice:NSNumber!
    var discountPrice:NSNumber?
    
    override init(dictionary: NSDictionary!) {
        if dictionary.count>0{
            isDiscountAvailable = dictionary.boolValueForKey("isDiscountAvailable")
            originalPrice = dictionary.numberObjectForKey("originalPrice")?.integerValue
            discountPrice = dictionary.numberObjectForKey("discountPrice")?.integerValue
        }
        super.init(dictionary: dictionary)
    }
    
    //MARK: - NSCoding -
    required convenience init(coder aDecoder: NSCoder) {
        self.init(dictionary: [String:AnyObject]())
        isDiscountAvailable = aDecoder.decodeObjectForKey("isDiscountAvailable") as! Bool
        originalPrice = aDecoder.decodeObjectForKey("originalPrice") as! NSNumber
        discountPrice = aDecoder.decodeObjectForKey("discountPrice") as? NSNumber
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(isDiscountAvailable, forKey: "isDiscountAvailable")
        aCoder.encodeObject(originalPrice, forKey: "originalPrice")
        aCoder.encodeObject(discountPrice, forKey: "discountPrice")
    }

}
