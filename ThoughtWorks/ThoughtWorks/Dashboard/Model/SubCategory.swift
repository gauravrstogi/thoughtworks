//
//  SubCategory.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class SubCategory: BaseModel {

    var id:SubCategoryID!
    var categoryId:CategoryID!
    var name:String!
    
    override init(dictionary: NSDictionary!) {
        if dictionary.count>0{
            id = SubCategoryID(rawValue: (dictionary["id"] as! NSNumber).integerValue)
            categoryId = CategoryID(rawValue: (dictionary["categoryId"] as! NSNumber).integerValue)
            name = dictionary["name"] as? String
        }
        super.init(dictionary: dictionary)
    }
    
    //MARK: - NSCoding -
    required convenience init(coder aDecoder: NSCoder) {
        self.init(dictionary: [String:AnyObject]())
        id = SubCategoryID(rawValue: aDecoder.decodeObjectForKey("id") as! Int)
        categoryId = CategoryID(rawValue: aDecoder.decodeObjectForKey("categoryId") as! Int)
        name = aDecoder.decodeObjectForKey("name") as? String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(categoryId.rawValue, forKey: "categoryId")
        aCoder.encodeObject(id.rawValue, forKey: "id")
        aCoder.encodeObject(name, forKey: "name")
    }
}
