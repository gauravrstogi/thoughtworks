//
//  Category.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Category: BaseModel {

    var categoryId:CategoryID!
    var categoryName:String?
    var categoryIcon:String?
    var categoryIconLarge:String?
    var tagLine:String?
    
    override init(dictionary: NSDictionary!) {
        if dictionary.count>0{
            categoryId = CategoryID(rawValue: (dictionary["categoryId"] as! NSNumber).integerValue)
            categoryName = dictionary["categoryName"] as? String
            categoryIcon = dictionary.stringObjectForKey("categoryIcon")
            categoryIconLarge = dictionary.stringObjectForKey("categoryIcon_large")
            tagLine = dictionary["tag_line"] as? String
        }
        super.init(dictionary: dictionary)
    }
    
    //MARK: - NSCoding -
    required convenience init(coder aDecoder: NSCoder) {
        self.init(dictionary: [String:AnyObject]())
        categoryName = aDecoder.decodeObjectForKey("categoryName") as? String
        categoryId = CategoryID(rawValue:aDecoder.decodeObjectForKey("categoryId") as! Int)
        categoryIcon = aDecoder.decodeObjectForKey("categoryIcon") as? String
        categoryIconLarge = aDecoder.decodeObjectForKey("categoryIconLarge") as? String
        tagLine = aDecoder.decodeObjectForKey("tagLine") as? String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(categoryId.rawValue, forKey: "categoryId")
        aCoder.encodeObject(categoryName, forKey: "categoryName")
        aCoder.encodeObject(categoryIcon, forKey: "categoryIcon")
        aCoder.encodeObject(categoryIconLarge, forKey: "categoryIconLarge")
        aCoder.encodeObject(tagLine, forKey: "tagLine")
    }
}
