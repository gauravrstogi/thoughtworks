//
//  Utility.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    class func findCategoryForId(categoryId:CategoryID)->Category?{
        let categories = Configuration.fetchSortedCategories()
        
        if let index = categories.indexOf({ (category:Category) -> Bool in
            return categoryId == category.categoryId
        }){
            return categories[index]
        }
        else{
            return nil
        }
        
    }
    
    class func findSubCategoryForId(subId:SubCategoryID)->SubCategory?{
        let categories = Configuration.fetchSortedSubCategories()
        if let index = categories.indexOf({ (subCategory:SubCategory) -> Bool in
            return subId == subCategory.id
        }){
            return categories[index]
        }
        else{
            return nil
        }
        
    }

}
