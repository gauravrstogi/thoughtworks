//
//  StorageManager.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 17/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class StorageManager: NSObject {
    
    class var sharedInstance: StorageManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: StorageManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = StorageManager()
        }
        return Static.instance!
    }
}
