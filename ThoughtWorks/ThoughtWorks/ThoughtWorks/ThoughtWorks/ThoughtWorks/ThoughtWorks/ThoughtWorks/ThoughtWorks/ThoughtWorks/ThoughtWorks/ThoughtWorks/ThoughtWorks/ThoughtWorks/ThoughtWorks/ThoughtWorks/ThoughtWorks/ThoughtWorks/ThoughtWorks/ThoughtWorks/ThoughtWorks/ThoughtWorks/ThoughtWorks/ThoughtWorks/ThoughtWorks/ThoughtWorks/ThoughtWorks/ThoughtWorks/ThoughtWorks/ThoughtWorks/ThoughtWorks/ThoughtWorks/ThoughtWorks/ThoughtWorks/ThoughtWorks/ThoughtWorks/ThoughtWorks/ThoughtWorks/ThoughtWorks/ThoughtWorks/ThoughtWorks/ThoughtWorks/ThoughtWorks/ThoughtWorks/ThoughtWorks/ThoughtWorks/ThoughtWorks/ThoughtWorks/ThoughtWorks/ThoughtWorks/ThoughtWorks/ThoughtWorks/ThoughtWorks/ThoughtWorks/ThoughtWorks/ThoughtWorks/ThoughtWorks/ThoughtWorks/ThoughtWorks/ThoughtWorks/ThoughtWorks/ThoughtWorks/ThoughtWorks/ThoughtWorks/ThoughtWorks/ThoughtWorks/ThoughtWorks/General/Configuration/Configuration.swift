//
//  Configuration.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 08/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import Foundation

class Configuration: NSObject {
    
    var configDict : NSDictionary!
    var currentEnvironmentDict : NSDictionary!
    
    class var sharedInstance: Configuration {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: Configuration? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = Configuration()
        }
        return Static.instance!
    }
    
    class func fetchSortedMenuItems(sortDes:NSSortDescriptor) -> [MenuOption]{
        let path = NSBundle.mainBundle().pathForResource("MenuOptions", ofType: "plist")
        var menuOptionsArray = [MenuOption]()
        let resultArray = NSArray(contentsOfFile: path!) as! [Dictionary<String, AnyObject>]
        for i in 0 ..< resultArray.count{
            if let json = resultArray[i] as NSDictionary! {
                menuOptionsArray.append(MenuOption(dictionary: json))
            }
        }
        let validOptions = (menuOptionsArray as NSArray).filter({
            let item = $0 as? MenuOption
            let number = item!.valueForKey(sortDes.key!) as! NSNumber
            return number.intValue >= 0
        })
        
        return (validOptions as NSArray).sortedArrayUsingDescriptors([sortDes]) as! [MenuOption]
    }
    
    class func fetchSortedCategories(sortDes:NSSortDescriptor? = nil) -> [Category]{
        let path = NSBundle.mainBundle().pathForResource("Category", ofType: "plist")
        var categories = [Category]()
        let resultArray = NSArray(contentsOfFile: path!) as! [Dictionary<String, AnyObject>]
        for i in 0 ..< resultArray.count{
            if let json = resultArray[i] as NSDictionary! {
                categories.append(Category(dictionary: json))
            }
        }
        
        if let sortDescriptor = sortDes{
            let validOptions = (categories as NSArray).filter({
                let item = $0 as? Category
                let number = (item!.valueForKey(sortDescriptor.key!) as! CategoryID).rawValue as NSNumber
                return number.intValue >= 0
            })
            
            return (validOptions as NSArray).sortedArrayUsingDescriptors([sortDescriptor]) as! [Category]
        }
        else{
            return categories
        }
    }
    
    class func fetchSortedSubCategories(sortDes:NSSortDescriptor? = nil) -> [SubCategory]{
        let path = NSBundle.mainBundle().pathForResource("SubCategory", ofType: "plist")
        var subCategories = [SubCategory]()
        let resultArray = NSArray(contentsOfFile: path!) as! [Dictionary<String, AnyObject>]
        for i in 0 ..< resultArray.count{
            if let json = resultArray[i] as NSDictionary! {
                subCategories.append(SubCategory(dictionary: json))
            }
        }
        
        if let sortDescriptor = sortDes{
            let validOptions = (subCategories as NSArray).filter({
                let item = $0 as? SubCategory
                let number = item!.valueForKey(sortDescriptor.key!) as! NSNumber
                return number.intValue >= 0
            })
            
            return (validOptions as NSArray).sortedArrayUsingDescriptors([sortDescriptor]) as! [SubCategory]
        }
        else{
            return subCategories
        }
    }
    
    class func fetchSortedProducts(sortDes:NSSortDescriptor? = nil) -> [Product]{
        let path = NSBundle.mainBundle().pathForResource("Product", ofType: "plist")
        var products = [Product]()
        let resultArray = NSArray(contentsOfFile: path!) as! [Dictionary<String, AnyObject>]
        for i in 0 ..< resultArray.count{
            if let json = resultArray[i] as NSDictionary! {
                products.append(Product(dictionary: json))
            }
        }
        
        if let sortDescriptor = sortDes{
            let validOptions = (products as NSArray).filter({
                let item = $0 as? Product
                let number = item!.valueForKey(sortDescriptor.key!) as! NSNumber
                return number.intValue >= 0
            })
            
            return (validOptions as NSArray).sortedArrayUsingDescriptors([sortDescriptor]) as! [Product]
        }
        else{
            return products
        }
    }
}
