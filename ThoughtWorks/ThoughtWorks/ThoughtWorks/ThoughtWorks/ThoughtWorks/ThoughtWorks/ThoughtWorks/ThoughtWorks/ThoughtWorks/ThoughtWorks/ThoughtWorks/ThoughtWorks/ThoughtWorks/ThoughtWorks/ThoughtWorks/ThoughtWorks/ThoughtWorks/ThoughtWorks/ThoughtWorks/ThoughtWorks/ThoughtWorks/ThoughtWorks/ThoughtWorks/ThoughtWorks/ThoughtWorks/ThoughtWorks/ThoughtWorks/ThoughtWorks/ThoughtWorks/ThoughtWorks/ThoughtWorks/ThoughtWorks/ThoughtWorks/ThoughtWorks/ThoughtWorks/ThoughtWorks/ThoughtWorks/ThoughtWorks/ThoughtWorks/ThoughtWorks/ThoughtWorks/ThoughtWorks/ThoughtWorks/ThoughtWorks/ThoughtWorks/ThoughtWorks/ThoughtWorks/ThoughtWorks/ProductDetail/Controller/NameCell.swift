//
//  NameCell.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class NameCell: UITableViewCell {

    @IBOutlet var nameLabel:UILabel!
    @IBOutlet var featuredIconImageView:UIImageView!
    
    func updateContentOfCell(product:Product){
        nameLabel.text = product.name
        
        if product.isFeatured == true{
            featuredIconImageView.hidden = false
        }
        else{
            featuredIconImageView.hidden = true
        }
    }
}
