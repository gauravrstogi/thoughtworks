//
//  UIUtility.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class UIUtility: NSObject {

    class func attributedStringForTagLine(tagline:String)->NSAttributedString{
        let str = NSMutableAttributedString()
        let stringComponents = tagline.componentsSeparatedByString(".")
        for index in 0 ..< stringComponents.count{
            let newStr = stringComponents[index]
            let newlineCharacter = NSAttributedString(string: "\n")
            if index == 0{
                let attributedOptions: [String: AnyObject] = [
                    NSFontAttributeName: UIFont.systemFontOfSize(12),
                    NSForegroundColorAttributeName: UIColor(netHex: 0x4098D1),
                    ]
                let newAttributedStr = NSMutableAttributedString(string: newStr)
                newAttributedStr.setAttributes(attributedOptions, range: NSMakeRange(0, newAttributedStr.length))
                str.appendAttributedString(newAttributedStr)
                str.appendAttributedString(newlineCharacter)
            }
            else if index == 1{
                let attributedOptions: [String: AnyObject] = [
                    NSFontAttributeName: UIFont.boldSystemFontOfSize(22),
                    NSForegroundColorAttributeName: UIColor(netHex: 0x343434),
                    ]
                let newAttributedStr = NSMutableAttributedString(string: newStr)
                newAttributedStr.setAttributes(attributedOptions, range: NSMakeRange(0, newAttributedStr.length))
                str.appendAttributedString(newAttributedStr)
                str.appendAttributedString(newlineCharacter)
            }
            else{
                let attributedOptions: [String: AnyObject] = [
                    NSFontAttributeName: UIFont.systemFontOfSize(16).boldItalic(),
                    NSForegroundColorAttributeName: UIColor(netHex: 0x343434),
                    ]
                let newAttributedStr = NSMutableAttributedString(string: newStr)
                newAttributedStr.setAttributes(attributedOptions, range: NSMakeRange(0, newAttributedStr.length))
                str.appendAttributedString(newAttributedStr)
            }
        }
        return str
    }
    
    class func attributedStringForPrice(price:Price)->NSAttributedString{
        let str = NSMutableAttributedString()
        if price.isDiscountAvailable == true{
            //For Discount Price
            let attributedOptionsDiscount: [String: AnyObject] = [
                NSFontAttributeName: UIFont.boldSystemFontOfSize(15),
                NSForegroundColorAttributeName: UIColor(netHex: 0xFF082B),
                ]
            let discountAttributedStr = NSMutableAttributedString(string: currency + String(price.discountPrice!))
            discountAttributedStr.setAttributes(attributedOptionsDiscount, range: NSMakeRange(0, discountAttributedStr.length))
            str.appendAttributedString(discountAttributedStr)
            
            //Adding additional space between two prices
            str.appendAttributedString(NSAttributedString(string: " "))
            
            //For Original Price
            let attributedOptionsOriginal: [String: AnyObject] = [
                NSFontAttributeName: UIFont.systemFontOfSize(13),
                NSForegroundColorAttributeName: UIColor(netHex: 0x000000),
                NSStrikethroughStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue,
                ]
            let originalAttributedStr = NSMutableAttributedString(string: currency + String(price.originalPrice))
            originalAttributedStr.setAttributes(attributedOptionsOriginal, range: NSMakeRange(0, originalAttributedStr.length))
            str.appendAttributedString(originalAttributedStr)
        }
        else{
            //For Original Price
            let attributedOptionsDiscount: [String: AnyObject] = [
                NSFontAttributeName: UIFont.boldSystemFontOfSize(15),
                NSForegroundColorAttributeName: UIColor(netHex: 0xFF082B),
                ]
            let discountAttributedStr = NSMutableAttributedString(string: currency + String(price.originalPrice))
            discountAttributedStr.setAttributes(attributedOptionsDiscount, range: NSMakeRange(0, discountAttributedStr.length))
            str.appendAttributedString(discountAttributedStr)
        }
        return str
    }
    
    class func attributedStringForDescription(text:String)->NSAttributedString{
        let str = NSMutableAttributedString()
        
        let attributedOptionsDiscount: [String: AnyObject] = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(16),
            NSForegroundColorAttributeName: UIColor(netHex: 0x000000),
            NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue,
            ]
        let headingStr = NSMutableAttributedString(string: "Description")
        headingStr.setAttributes(attributedOptionsDiscount, range: NSMakeRange(0, headingStr.length))
        str.appendAttributedString(headingStr)
        
        //Adding new line
        str.appendAttributedString(NSAttributedString(string: "\n\n"))
        
        let attributedOptionsOriginal: [String: AnyObject] = [
            NSFontAttributeName: UIFont.systemFontOfSize(13),
            NSForegroundColorAttributeName: UIColor(netHex: 0x000000),
            ]
        let descriptionAttributedStr = NSMutableAttributedString(string: text)
        descriptionAttributedStr.setAttributes(attributedOptionsOriginal, range: NSMakeRange(0, descriptionAttributedStr.length))
        str.appendAttributedString(descriptionAttributedStr)
        
        return str
    }
    
    class func attributedStringForCartSubtotal(numberOfItem:Int32, totalPrice:Float32)->NSAttributedString{
        let str = NSMutableAttributedString()
        
        let attributedOptions: [String: AnyObject] = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(16),
            NSForegroundColorAttributeName: UIColor(netHex: 0x000000),
            ]
        let headingStr = NSMutableAttributedString(string: "Cart Subtotal (" + String(numberOfItem) + (numberOfItem < 2 ? " item":" items") + "): ")
        headingStr.setAttributes(attributedOptions, range: NSMakeRange(0, headingStr.length))
        str.appendAttributedString(headingStr)
        
        //For Price
        let attributedOptionsPrice: [String: AnyObject] = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(15),
            NSForegroundColorAttributeName: UIColor(netHex: 0xFF082B),
            ]
        let discountAttributedStr = NSMutableAttributedString(string: currency + String(totalPrice))
        discountAttributedStr.setAttributes(attributedOptionsPrice, range: NSMakeRange(0, discountAttributedStr.length))
        str.appendAttributedString(discountAttributedStr)
        
        return str
    }
    
    class func performTask(task:(()->Void), afterDelay:Double)->Void{
        let seconds = afterDelay
        let delay = seconds * Double(NSEC_PER_SEC)  // nanoseconds per seconds
        let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        dispatch_after(dispatchTime, dispatch_get_main_queue(), {
            task()
        })
    }
}
