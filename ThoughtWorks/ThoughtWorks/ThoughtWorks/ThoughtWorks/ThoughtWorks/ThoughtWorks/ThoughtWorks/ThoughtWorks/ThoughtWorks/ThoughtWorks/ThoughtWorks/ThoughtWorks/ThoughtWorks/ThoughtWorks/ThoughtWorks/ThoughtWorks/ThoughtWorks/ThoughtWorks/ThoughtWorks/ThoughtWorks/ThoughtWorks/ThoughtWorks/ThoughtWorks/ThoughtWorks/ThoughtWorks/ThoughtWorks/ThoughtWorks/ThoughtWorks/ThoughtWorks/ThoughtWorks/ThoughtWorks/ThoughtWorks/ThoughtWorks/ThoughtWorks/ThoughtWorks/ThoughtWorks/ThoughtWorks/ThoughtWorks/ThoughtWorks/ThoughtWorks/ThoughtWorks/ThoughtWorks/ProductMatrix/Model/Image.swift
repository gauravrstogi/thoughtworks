//
//  ProductImage.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Image: BaseModel {
    var url:NSURL!
    var isDefault:Bool = false
    
    override init(dictionary: NSDictionary!) {
        
        if let string = dictionary.stringObjectForKey("url"){
            url = NSURL(string: string)
        }
        
        isDefault = dictionary.boolValueForKey("isDefault")
        super.init(dictionary:dictionary)
    }
}
