//
//  ShoppingBagManager.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class ShoppingBagManager: NSObject {
    
    private var shoppingList:[Product] = []
    
    class var sharedInstance: ShoppingBagManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: ShoppingBagManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = ShoppingBagManager()
        }
        return Static.instance!
    }
    
    func addItemToTheBag(product:Product, controller:BaseViewController? = nil){
        if let index = shoppingList.indexOf({(object:Product)-> Bool in
            return product.sku == object.sku
        }){
            //Product Already Exist
            let foundProduct = shoppingList[index] as Product
            foundProduct.itemCount  = foundProduct.itemCount.integerValue + 1
        }
        else{
            self.shoppingList.append(product)
            
        }
        
        if let viewController = controller{
            viewController.updateShoppingBagBadgeCount()
        }
        
    }
    
    func removeItemFromShoppingBag(product:Product, controller:BaseViewController? = nil){
        self.shoppingList.remove(product)
        
        if let viewController = controller{
            viewController.updateShoppingBagBadgeCount()
        }
    }
    
    func allItem()->[Product]{
        return self.shoppingList
    }
}
