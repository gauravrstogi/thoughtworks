//
//  Array+Extensions.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 16/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

extension RangeReplaceableCollectionType where Generator.Element : Equatable {
    
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object : Generator.Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
}