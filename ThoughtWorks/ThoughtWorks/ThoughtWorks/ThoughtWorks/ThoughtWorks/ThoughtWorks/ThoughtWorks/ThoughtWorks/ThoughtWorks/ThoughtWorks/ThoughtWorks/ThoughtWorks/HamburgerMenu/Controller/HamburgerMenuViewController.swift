//
//  HamburgerMenuViewController.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 12/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class HamburgerMenuViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource{

    var menuOptions:[MenuOption]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let optionSortDescriptor = NSSortDescriptor(key: "menuPosition", ascending: true)
        menuOptions = Configuration.fetchSortedMenuItems(optionSortDescriptor)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if (self.navigationItem.hidesBackButton) {
            var frame = self.navigationItem.titleView!.frame;
            frame.origin.x = 20;
            self.navigationItem.titleView!.frame = frame;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableView Delegate & DataSource Methods -
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let options = self.menuOptions{
            return options.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:MenuOptionCell = tableView.dequeueReusableCellWithIdentifier("menuOptionCell")! as! MenuOptionCell
        let menuOption : MenuOption = self.menuOptions![indexPath.row]
        if let optionImageStr = menuOption.optionImage
        {
            cell.optionImageView.image = UIImage(named: optionImageStr)
        }
        else
        {
            cell.optionImageView.image = nil
        }
        cell.optionNameLabel.text = menuOption.title
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let menuOption = self.menuOptions![indexPath.row]
        
        switch menuOption.optionId {
        case .Dashboard:
            break
        case .Featured:
            break
        case .Electronics:
            break
        case .Furniture:
            break
        }
        self.revealViewController().revealToggleAnimated(true)
    }

}
