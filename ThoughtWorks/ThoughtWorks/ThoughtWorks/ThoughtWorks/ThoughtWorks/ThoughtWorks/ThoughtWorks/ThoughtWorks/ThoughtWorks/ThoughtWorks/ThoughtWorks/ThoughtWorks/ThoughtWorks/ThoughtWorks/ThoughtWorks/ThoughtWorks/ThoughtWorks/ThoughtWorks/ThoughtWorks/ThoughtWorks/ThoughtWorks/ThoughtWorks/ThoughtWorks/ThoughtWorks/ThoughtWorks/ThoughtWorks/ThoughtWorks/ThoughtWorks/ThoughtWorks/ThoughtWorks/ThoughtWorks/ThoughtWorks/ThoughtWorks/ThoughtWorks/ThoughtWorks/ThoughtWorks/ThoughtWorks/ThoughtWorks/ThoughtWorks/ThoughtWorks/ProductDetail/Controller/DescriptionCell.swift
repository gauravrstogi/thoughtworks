//
//  DescriptionCell.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell {

    @IBOutlet var descriptionLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateContentOfCell(text:String){
        descriptionLabel.attributedText = UIUtility.attributedStringForDescription(text)
    }
}
