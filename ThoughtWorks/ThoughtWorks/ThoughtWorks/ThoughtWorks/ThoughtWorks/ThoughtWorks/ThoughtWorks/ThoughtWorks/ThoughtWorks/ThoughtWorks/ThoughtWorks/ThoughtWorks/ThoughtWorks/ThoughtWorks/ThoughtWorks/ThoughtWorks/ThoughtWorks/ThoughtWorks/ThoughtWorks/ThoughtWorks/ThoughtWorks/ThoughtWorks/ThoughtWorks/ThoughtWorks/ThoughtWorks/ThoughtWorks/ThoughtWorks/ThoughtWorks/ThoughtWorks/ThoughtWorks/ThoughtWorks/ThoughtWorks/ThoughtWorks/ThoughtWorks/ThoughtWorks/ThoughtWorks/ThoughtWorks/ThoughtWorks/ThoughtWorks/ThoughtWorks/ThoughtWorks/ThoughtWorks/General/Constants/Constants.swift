//
//  Constants.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

enum OptionID: Int{
    case Dashboard = 0,
    Featured,
    Electronics,
    Furniture
}

enum CategoryID: Int{
    case Featured=0,
    Electronics,
    Furniture
}

enum SubCategoryID: Int{
    case MicrowaveOven=0,
    Television,
    VacuumCleaner,
    Table,
    Chair,
    Almirah
}

enum DetailScreenSection: Int{
    case ProductName = 0,
    ImageScroller,
    Price,
    Description,
    AddToBag,
    //It should be in the last
    SectionsCount
}

var currency:String{
get{
    return "Rs."
}
}

var screenWidth:CGFloat{
get{
    return (UIApplication.sharedApplication().windows.first?.frame.width)!
}
}

var screenHeight:CGFloat{
get{
    return (UIApplication.sharedApplication().windows.first?.frame.height)!
}

}

func localize (key:String) ->String
{
    return NSLocalizedString(key, comment: "")
}