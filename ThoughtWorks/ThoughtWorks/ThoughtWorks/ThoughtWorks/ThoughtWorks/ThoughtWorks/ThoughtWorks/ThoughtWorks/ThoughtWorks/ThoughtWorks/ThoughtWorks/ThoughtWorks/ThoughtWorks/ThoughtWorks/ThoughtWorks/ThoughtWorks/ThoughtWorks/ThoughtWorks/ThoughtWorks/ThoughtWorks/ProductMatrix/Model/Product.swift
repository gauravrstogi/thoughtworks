//
//  Product.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Product: BaseModel {
    var sku:String
    var name:String
    var category:Category?
    var itemCount:NSNumber
    var subCategory:SubCategory?
    var productDescription:String?
    var price:Price!
    var images:[Image] = []
    var isFeatured:Bool = false
    
    override init(dictionary: NSDictionary!) {
        sku = dictionary["sku"] as! String
        name = dictionary["name"] as! String
        itemCount = dictionary.numberObjectForKey("itemCount")! as NSNumber
        if let categoryId = CategoryID(rawValue: (dictionary["categoryId"] as! NSNumber).integerValue){
            category = Product.findCategoryForId(categoryId)
        }
        if let subCategoryId = SubCategoryID(rawValue: (dictionary["sub_categoryId"] as! NSNumber).integerValue){
            subCategory = Product.findSubCategoryForId(subCategoryId)
        }
        productDescription = dictionary.stringObjectForKey("productDescription")
        if let priceDict = dictionary.dictionaryObjectForKey("price"){
            price = Price(dictionary: priceDict)
        }
        
        if let list = dictionary.objectForKey("images") as? NSArray{
            for object in list{
                images.append(Image(dictionary: object as! NSDictionary))
            }
        }
        isFeatured = (dictionary["is_featured"] as! NSNumber).boolValue
        super.init(dictionary: dictionary)
    }
    
    class func findCategoryForId(categoryId:CategoryID)->Category?{
        let categories = Configuration.fetchSortedCategories()
        
        if let index = categories.indexOf({ (category:Category) -> Bool in
            return categoryId == category.categoryId
        }){
            return categories[index]
        }
        else{
            return nil
        }
        
    }
    
    class func findSubCategoryForId(subId:SubCategoryID)->SubCategory?{
        let categories = Configuration.fetchSortedSubCategories()
        if let index = categories.indexOf({ (subCategory:SubCategory) -> Bool in
            return subId == subCategory.id
        }){
            return categories[index]
        }
        else{
            return nil
        }
        
    }
}
