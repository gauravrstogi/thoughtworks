//
//  ProductMatrixCell.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import SDWebImage

class ProductMatrixCell: UICollectionViewCell {
    @IBOutlet var productImageView:UIImageView!
    @IBOutlet var priceLabel:UILabel!
    @IBOutlet var productNameLabel:UILabel!
    @IBOutlet var featuredImageView:UIImageView!
    @IBOutlet var addToBagBtn:UIButton!
    
    var didClickAddToBag : ((sender:UIButton)->Void)?
    
    override func awakeFromNib() {
        self.contentView.layer.borderWidth = 1.0
        self.contentView.layer.borderColor = UIColor.blackColor().CGColor
        
        if let button = self.addToBagBtn{
            button.layer.borderWidth = 1.0
            button.layer.borderColor = UIColor.blackColor().CGColor
        }
    }
    
    @IBAction func didClickAddToBag(sender: UIButton) {
        if let closure = self.didClickAddToBag{
            closure(sender: sender)
        }
    }
    
    func updateContentOfCell(product:Product){
        if (product.price != nil){
            priceLabel.attributedText = UIUtility.attributedStringForPrice(product.price)
        }
        
        productNameLabel.text = product.name
        if let index = product.images.indexOf({ (object:Image) -> Bool in
            return object.isDefault == true
        }){
            productImageView.sd_setImageWithURL(product.images[index].url, placeholderImage: UIImage(named: "placeholder-medium"), options: SDWebImageOptions.LowPriority)
        }
        else{
            productImageView.image = UIImage(named: "placeholder-medium")
        }
        
        if product.isFeatured == true{
            featuredImageView.hidden = false
        }
        else{
            featuredImageView.hidden = true
        }
    }
}
