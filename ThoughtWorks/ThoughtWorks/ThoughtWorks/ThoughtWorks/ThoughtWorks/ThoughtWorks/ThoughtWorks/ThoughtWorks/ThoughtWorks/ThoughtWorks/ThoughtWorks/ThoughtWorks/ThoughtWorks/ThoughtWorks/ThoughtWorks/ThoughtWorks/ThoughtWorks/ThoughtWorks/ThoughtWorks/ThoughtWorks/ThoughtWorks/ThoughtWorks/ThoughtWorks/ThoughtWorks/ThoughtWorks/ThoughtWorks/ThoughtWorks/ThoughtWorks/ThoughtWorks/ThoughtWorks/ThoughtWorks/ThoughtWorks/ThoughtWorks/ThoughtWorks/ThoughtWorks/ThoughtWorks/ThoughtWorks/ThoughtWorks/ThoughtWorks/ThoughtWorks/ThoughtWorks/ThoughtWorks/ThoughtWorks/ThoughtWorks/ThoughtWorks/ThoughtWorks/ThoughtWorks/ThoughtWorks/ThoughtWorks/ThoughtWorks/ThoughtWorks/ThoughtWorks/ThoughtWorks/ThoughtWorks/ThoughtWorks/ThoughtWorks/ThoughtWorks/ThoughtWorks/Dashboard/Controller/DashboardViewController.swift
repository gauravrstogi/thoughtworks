//
//  DashboardViewController.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 12/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import SWRevealViewController

class DashboardViewController: BaseViewController, SWRevealViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var categoryTableView: UITableView!
    @IBOutlet var menuButton:UIBarButtonItem!
    var dataList:[Category] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customSetup()
        self.loadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.updateShoppingBagBadgeCount()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController .isKindOfClass(ProductMatrixViewController)
        {
            let productMatrixViewController = segue.destinationViewController as! ProductMatrixViewController
            if segue.identifier == "showProductMatrix"{
                let indexPath:NSIndexPath = self.categoryTableView.indexPathForSelectedRow!
                productMatrixViewController.category = self.dataList[indexPath.row]
                switch self.dataList[indexPath.row].categoryId! {
                case .Featured:
                    productMatrixViewController.products = CacheManager.sharedInstance.featuredProducts
                    break
                case .Electronics,.Furniture:
                    productMatrixViewController.products = CacheManager.sharedInstance.productsForCategory(self.dataList[indexPath.row])
                    break
                }
            }
        }
    }
    
    func customSetup(){
        if revealViewController() != nil{
            revealViewController().rearViewRevealWidth = (screenWidth/5)*4
            revealViewController().delegate = self
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        self.addShoppingBagLeftBarButton()
    }
    
    func loadData(){
        dataList = Configuration.fetchSortedCategories()
        self.categoryTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:DashboardCell = tableView.dequeueReusableCellWithIdentifier("dashboardCell") as! DashboardCell
        cell.updateContentOfCell(dataList[indexPath.row])
        return cell
    }
}
