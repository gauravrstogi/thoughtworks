//
//  Category.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Category: BaseModel {

    var categoryId:CategoryID!
    var categoryName:String
    var categoryIcon:String?
    var categoryIconLarge:String?
    var tagLine:String?
    
    override init(dictionary: NSDictionary!) {
        categoryId = CategoryID(rawValue: (dictionary["categoryId"] as! NSNumber).integerValue)
        categoryName = dictionary["categoryName"] as! String
        categoryIcon = dictionary.stringObjectForKey("categoryIcon")
        categoryIconLarge = dictionary.stringObjectForKey("categoryIcon_large")
        tagLine = dictionary["tag_line"] as? String
        super.init(dictionary: dictionary)
    }
}
