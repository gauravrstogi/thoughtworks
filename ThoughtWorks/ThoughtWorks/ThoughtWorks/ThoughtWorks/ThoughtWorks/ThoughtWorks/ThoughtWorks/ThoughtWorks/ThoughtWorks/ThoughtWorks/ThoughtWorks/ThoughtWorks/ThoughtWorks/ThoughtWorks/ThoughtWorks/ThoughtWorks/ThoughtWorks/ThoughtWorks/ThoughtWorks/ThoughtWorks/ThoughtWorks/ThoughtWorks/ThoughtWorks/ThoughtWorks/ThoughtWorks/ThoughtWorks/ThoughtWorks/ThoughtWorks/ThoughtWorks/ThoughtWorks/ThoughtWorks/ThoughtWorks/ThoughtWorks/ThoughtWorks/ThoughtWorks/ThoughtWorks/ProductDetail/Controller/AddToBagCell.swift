//
//  AddToBagCell.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 16/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class AddToBagCell: UITableViewCell {

    @IBOutlet var buyNowBtn: UIButton!
    @IBOutlet var addToBagBtn: UIButton!
    
    var didClickBuyNow:((sender:UIButton)->Void)?
    var didClickAddToBag:((sender:UIButton)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if let button = self.addToBagBtn{
            button.layer.borderWidth = 1.0
            button.layer.borderColor = UIColor.blackColor().CGColor
        }
        
        if let button = self.buyNowBtn{
            button.layer.borderWidth = 1.0
            button.layer.borderColor = UIColor.blackColor().CGColor
        }
    }

    @IBAction func didClickBuyNow(sender: UIButton) {
        if let closure = self.didClickBuyNow{
            closure(sender: sender)
        }
    }
    
    @IBAction func didClickAddToBag(sender: UIButton) {
        if let closure = self.didClickAddToBag{
            closure(sender: sender)
        }
    }
}
