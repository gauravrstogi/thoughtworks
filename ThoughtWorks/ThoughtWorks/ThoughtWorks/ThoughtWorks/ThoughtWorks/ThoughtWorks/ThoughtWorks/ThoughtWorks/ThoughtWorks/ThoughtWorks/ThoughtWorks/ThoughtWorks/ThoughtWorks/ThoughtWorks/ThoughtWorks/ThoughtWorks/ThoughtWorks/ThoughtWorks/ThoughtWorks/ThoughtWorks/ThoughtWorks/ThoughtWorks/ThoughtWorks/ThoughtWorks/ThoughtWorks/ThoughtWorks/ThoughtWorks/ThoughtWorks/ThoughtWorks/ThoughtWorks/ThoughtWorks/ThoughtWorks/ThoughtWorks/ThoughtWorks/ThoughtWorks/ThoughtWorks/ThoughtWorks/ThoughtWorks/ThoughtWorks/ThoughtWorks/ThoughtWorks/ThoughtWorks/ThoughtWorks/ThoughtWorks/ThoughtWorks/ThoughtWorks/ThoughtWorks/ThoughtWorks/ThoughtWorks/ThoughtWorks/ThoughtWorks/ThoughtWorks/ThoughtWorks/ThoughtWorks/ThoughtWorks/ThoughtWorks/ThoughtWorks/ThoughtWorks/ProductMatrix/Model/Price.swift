//
//  Price.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class Price: BaseModel {
    
    var isDiscountAvailable:Bool = false
    var originalPrice:NSNumber!
    var discountPrice:NSNumber?
    
    override init(dictionary: NSDictionary!) {
        isDiscountAvailable = dictionary.boolValueForKey("isDiscountAvailable")
        originalPrice = dictionary.numberObjectForKey("originalPrice")?.integerValue
        discountPrice = dictionary.numberObjectForKey("discountPrice")?.integerValue
        super.init(dictionary: dictionary)
    }
}
