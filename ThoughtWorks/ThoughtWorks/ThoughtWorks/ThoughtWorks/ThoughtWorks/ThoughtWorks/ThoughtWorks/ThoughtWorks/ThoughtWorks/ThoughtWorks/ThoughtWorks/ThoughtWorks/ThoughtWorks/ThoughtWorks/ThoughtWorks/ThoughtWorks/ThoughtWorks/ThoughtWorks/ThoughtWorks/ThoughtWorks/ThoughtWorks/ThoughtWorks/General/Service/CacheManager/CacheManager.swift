//
//  CacheManager.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class CacheManager: NSObject {
    class var sharedInstance: CacheManager {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: CacheManager? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = CacheManager()
        }
        return Static.instance!
    }
    
    var allProducts:[Product]{
        get{
            return Configuration.fetchSortedProducts()
        }
    }
    
    var featuredProducts:[Product]{
        get{
            return allProducts.filter({ (object:Product) -> Bool in
                return object.isFeatured == true
            })
        }
    }
    
    func productsForCategory(category:Category)->[Product]{
        let filteredProducts = allProducts.filter { (object:Product) -> Bool in
            return object.category?.categoryId == category.categoryId
        }
        return filteredProducts
    }
}
