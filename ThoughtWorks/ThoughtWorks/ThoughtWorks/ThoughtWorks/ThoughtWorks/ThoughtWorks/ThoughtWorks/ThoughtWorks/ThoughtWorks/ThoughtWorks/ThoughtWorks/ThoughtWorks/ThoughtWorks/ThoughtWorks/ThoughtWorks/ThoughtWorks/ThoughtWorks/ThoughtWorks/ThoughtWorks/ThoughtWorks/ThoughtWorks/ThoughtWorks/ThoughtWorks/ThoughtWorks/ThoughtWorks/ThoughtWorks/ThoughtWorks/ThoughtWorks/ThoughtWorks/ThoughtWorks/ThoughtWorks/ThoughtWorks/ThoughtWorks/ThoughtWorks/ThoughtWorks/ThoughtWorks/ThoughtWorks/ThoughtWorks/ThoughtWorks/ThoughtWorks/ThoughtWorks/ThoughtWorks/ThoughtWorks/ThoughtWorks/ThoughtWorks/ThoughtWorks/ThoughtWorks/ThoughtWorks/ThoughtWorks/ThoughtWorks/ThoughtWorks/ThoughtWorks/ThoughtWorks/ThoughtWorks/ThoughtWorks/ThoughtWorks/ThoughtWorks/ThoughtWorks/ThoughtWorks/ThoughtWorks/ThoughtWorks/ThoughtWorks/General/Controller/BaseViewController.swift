//
//  BaseViewController.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 12/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
#import "UIBarButtonItem+Badge.h"

class BaseViewController: UIViewController {

    var lastNavColor:UIColor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem?.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        self.navigationItem.rightBarButtonItem?.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        lastNavColor = self.navigationController?.navigationBar.barTintColor
        self.navigationController?.navigationBar.barTintColor = self.navigationBarColor()
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = lastNavColor
        super.viewWillDisappear(animated)
    }
    
    @IBAction func didClickLeftNavigationBarButon(sender: UIBarButtonItem) {
        
    }
    
    @IBAction func didClickBackNavigationBarButon(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func updateShoppingBagBadgeCount(){
        self.navigationItem.rightBarButtonItem!.badgeValue = String(ShoppingBagManager.sharedInstance.allItem().count)
    }
    
    func addShoppingBagLeftBarButton(){
        self.navigationItem.rightBarButtonItem = self.shoppingBarBarButton()
    }
    
    func shoppingBarBarButton()->UIBarButtonItem{
        let image:UIImage = UIImage(named: "shopping-bag")!
        let button:UIButton = UIButton(type: UIButtonType.Custom)
        button.setImage(image, forState: UIControlState.Normal)
        button.frame = CGRectMake(0, 0, image.size.width, image.size.height)
        button.addTarget(self, action: #selector(openShoppingBag), forControlEvents: UIControlEvents.TouchUpInside)
        
        let barButton:UIBarButtonItem = UIBarButtonItem(customView: button)
        return barButton
    }
    
    func openShoppingBag(){
        let shoppingBagStoryboard = UIStoryboard(name: "ShoppingBag", bundle: nil)
        let shoppingBagViewController = shoppingBagStoryboard.instantiateInitialViewController() as! ShoppingBagViewController
        self.navigationController?.pushViewController(shoppingBagViewController, animated: true)
    }
    
    func navigationBarColor() -> UIColor{
        return UIColor(netHex: 0xFFFFFF)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
