//
//  SubCategory.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class SubCategory: BaseModel {

    var id:SubCategoryID!
    var categoryId:CategoryID!
    var name:String!
    
    override init(dictionary: NSDictionary!) {
        id = SubCategoryID(rawValue: (dictionary["id"] as! NSNumber).integerValue)
        categoryId = CategoryID(rawValue: (dictionary["categoryId"] as! NSNumber).integerValue)
        name = dictionary["name"] as? String
        super.init(dictionary: dictionary)
    }
}
