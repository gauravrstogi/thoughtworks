//
//  ProductMatrixViewController.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 12/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProductMatrixViewController: BaseViewController,UICollectionViewDelegate, UICollectionViewDataSource {

    var category:Category!
    var products:[Product] = []
    @IBOutlet var productsCollection:UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customSetup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.updateShoppingBagBadgeCount()
    }
    
    func customSetup(){
        self.navigationItem.title = category.categoryName
        self.addShoppingBagLeftBarButton()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController .isKindOfClass(ProductDetailViewController)
        {
            let productDetailViewController = segue.destinationViewController as! ProductDetailViewController
            if segue.identifier == "showProductDetail"{
                let indexPath:NSIndexPath = (self.productsCollection.indexPathsForSelectedItems()?.first)!
                productDetailViewController.product = products[indexPath.row]
            }
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView){
        if products.count>0{
            let translation : CGPoint = scrollView.panGestureRecognizer.translationInView(scrollView.superview!)
            if(translation.y >= 0){
                //scroll down
                if ((self.navigationController?.navigationBarHidden) != nil){
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                    self.setNeedsStatusBarAppearanceUpdate()
                }
            }
            else{
                //scroll up
                if ((self.navigationController?.navigationBarHidden) != nil){
                    self.navigationController?.setNavigationBarHidden(true, animated: true)
                    self.setNeedsStatusBarAppearanceUpdate()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:ProductMatrixCell = collectionView.dequeueReusableCellWithReuseIdentifier("productMatrixCell", forIndexPath: indexPath) as! ProductMatrixCell
        cell.didClickAddToBag = {[unowned self](sender)->Void in
            let progressHUD:MBProgressHUD = MBProgressHUD()
            progressHUD.label.text = "Shopping Bag"
            progressHUD.detailsLabel.text = "Adding item to shopping bag , please wait .."
            progressHUD.center = self.view.center
            self.view.addSubview(progressHUD)
            progressHUD.showAnimated(true)
            UIUtility.performTask({ 
                ShoppingBagManager.sharedInstance.addItemToTheBag(self.products[indexPath.row],controller: self)
                progressHUD.hideAnimated(true)
                }, afterDelay: 2.0)
        }
        cell.updateContentOfCell(products[indexPath.row])
        return cell
    }
    
}
