//
//  DashboardCell.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class DashboardCell: UITableViewCell {

    @IBOutlet var iconImageView:UIImageView!
    @IBOutlet var tagLineLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateContentOfCell(category:Category)->Void{
        if let icon = category.categoryIconLarge{
            self.iconImageView.image = UIImage(named: icon)
        }
        
        if let string = category.tagLine{
            tagLineLabel.attributedText = UIUtility.attributedStringForTagLine(string)
        }
    }
}
