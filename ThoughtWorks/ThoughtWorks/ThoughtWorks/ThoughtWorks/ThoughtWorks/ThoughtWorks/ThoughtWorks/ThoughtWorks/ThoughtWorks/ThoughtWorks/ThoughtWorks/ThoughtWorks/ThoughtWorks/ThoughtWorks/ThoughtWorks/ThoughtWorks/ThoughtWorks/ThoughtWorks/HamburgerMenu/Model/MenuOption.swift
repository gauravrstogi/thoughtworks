//
//  MenuOption.swift
//  Teamwork
//
//  Created by Gaurav Rastogi on 09/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class MenuOption: BaseModel {
    var optionId : OptionID
    var title : String
    var optionImage : String?
    var menuPosition : Int8
    
    override init(dictionary: NSDictionary!) {
        optionId = OptionID(rawValue: (dictionary["optionId"] as! NSNumber).integerValue)!
        title = dictionary["title"] as! String
        optionImage = dictionary.stringObjectForKey("optionImage")
        menuPosition = Int8((dictionary["menuPosition"] as! NSNumber).integerValue)
        super.init(dictionary: dictionary)
    }
}
