//
//  PriceCell.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class PriceCell: UITableViewCell {

    @IBOutlet var priceLabel:UILabel!
    @IBOutlet var discountLabel:UILabel!
        
    func updateContentOfCell(price:Price){
        priceLabel.attributedText = UIUtility.attributedStringForPrice(price)
        if price.isDiscountAvailable == true{
            let discountPrice = price.discountPrice?.doubleValue
            let originalPrice = price.originalPrice.doubleValue
            let percentageOff:Double = ((originalPrice - discountPrice!) / originalPrice) * 100
            discountLabel.text = String(Int(percentageOff)) + "% Discount, You Save " + currency +  String(price.originalPrice.integerValue - (price.discountPrice?.integerValue)!)
        }
    }
}
