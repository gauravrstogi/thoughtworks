//
//  ImageCell.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import SDWebImage

class ImageCell: UICollectionViewCell {
    @IBOutlet var imageView:UIImageView!
    
    func updateContentOfCell(image:Image){
        imageView.sd_setImageWithURL(image.url, placeholderImage: UIImage(named: "placeholder-large"), options: SDWebImageOptions.LowPriority)
    }
    
    class func size()->CGSize{
        return CGSizeMake(screenWidth-20,300.0)
    }
}
