//
//  ProductDetailViewController.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 12/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import MBProgressHUD

class ProductDetailViewController: BaseViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var productDetailTableView: UITableView!
    var product:Product!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customSetup()
        // Do any additional setup after loading the view.
    }
    
    func customSetup(){
        self.productDetailTableView.estimatedRowHeight = 88.0
        self.productDetailTableView.rowHeight = UITableViewAutomaticDimension
        self.addShoppingBagLeftBarButton()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView){
        let translation : CGPoint = scrollView.panGestureRecognizer.translationInView(scrollView.superview!)
        if(translation.y >= 0){
            //scroll down
            if ((self.navigationController?.navigationBarHidden) != nil){
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                self.setNeedsStatusBarAppearanceUpdate()
            }
        }
        else{
            //scroll up
            if ((self.navigationController?.navigationBarHidden) != nil){
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                self.setNeedsStatusBarAppearanceUpdate()
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.updateShoppingBagBadgeCount()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return DetailScreenSection.SectionsCount.rawValue + 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch DetailScreenSection(rawValue: section)! {
        case .ProductName:
            return 1
        case .ImageScroller:
            return 1
        case .Price:
            return 1
        case .Description:
            return 1
        case .AddToBag:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch DetailScreenSection(rawValue: indexPath.section)! {
        case .ProductName:
            let cell:NameCell = tableView.dequeueReusableCellWithIdentifier("nameCell") as! NameCell
            cell.updateContentOfCell(self.product)
            return cell
        case .ImageScroller:
            let cell:ImageScrollerCell = tableView.dequeueReusableCellWithIdentifier("imageScrollerCell") as! ImageScrollerCell
            cell.updateContentOfCell(self.product.images)
            return cell
        case .Price:
            let cell:PriceCell = tableView.dequeueReusableCellWithIdentifier("priceCell") as! PriceCell
            cell.updateContentOfCell(self.product.price)
            return cell
        case .Description:
            let cell:DescriptionCell = tableView.dequeueReusableCellWithIdentifier("descriptionCell") as! DescriptionCell
            cell.updateContentOfCell(self.product.productDescription!)
            return cell
        case .AddToBag:
            let cell:AddToBagCell = tableView.dequeueReusableCellWithIdentifier("addToBagCell") as! AddToBagCell
            cell.didClickAddToBag = {[unowned self](sender)->Void in
                let progressHUD:MBProgressHUD = MBProgressHUD()
                progressHUD.label.text = "Shopping Bag"
                progressHUD.detailsLabel.text = "Adding item to shopping bag , please wait .."
                progressHUD.center = self.view.center
                self.view.addSubview(progressHUD)
                progressHUD.showAnimated(true)
                UIUtility.performTask({ 
                    ShoppingBagManager.sharedInstance.addItemToTheBag(self.product,controller: self)
                    progressHUD.hideAnimated(true)
                    }, afterDelay: 2.0)
            }
            cell.didClickBuyNow = {[unowned self](sender)->Void in
                let progressHUD:MBProgressHUD = MBProgressHUD()
                progressHUD.label.text = "Checking Out"
                progressHUD.detailsLabel.text = "Please wait .."
                progressHUD.center = self.view.center
                self.view.addSubview(progressHUD)
                progressHUD.showAnimated(true)
                UIUtility.performTask({
                    progressHUD.hideAnimated(true)
                    }, afterDelay: 2.0)
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch DetailScreenSection(rawValue: indexPath.section)! {
        case .ProductName:
            return UITableViewAutomaticDimension
        case .ImageScroller:
            return 360.0
        case .Price:
            return UITableViewAutomaticDimension
        case .Description:
            return UITableViewAutomaticDimension
        case .AddToBag:
            return UITableViewAutomaticDimension
        default:
            return 0
        }
    }
}
