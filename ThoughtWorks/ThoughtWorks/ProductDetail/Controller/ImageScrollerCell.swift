//
//  ImageScrollerCell.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit

class ImageScrollerCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    var images:[Image] = []
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var imagesCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.pageControl.numberOfPages = images.count
        if let index = images.indexOf({(image:Image)->Bool in
            return image.isDefault == true
        }){
            self.pageControl.currentPage = index
            self.imagesCollectionView.scrollToItemAtIndexPath(NSIndexPath(forRow:index,inSection: 0) , atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
        }
    }
    
    func updateContentOfCell(images:[Image]){
        self.images = images
        self.imagesCollectionView.reloadData()
    }

    //MARK: - UICollectionView Delegate & DataSource Methods
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell:ImageCell = collectionView.dequeueReusableCellWithReuseIdentifier("imageCell", forIndexPath: indexPath) as! ImageCell
        self.pageControl.currentPage = indexPath.row
        cell.updateContentOfCell(images[indexPath.row])
        return cell
    }
    
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAtIndexPath indexPath: NSIndexPath) ->CGSize {
        return ImageCell.size()
    }
    
}
