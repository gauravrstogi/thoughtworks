//
//  ShoppingBagViewController.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 12/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import MBProgressHUD

class ShoppingBagViewController: BaseViewController,UITableViewDelegate, UITableViewDataSource {
    
    var bagItems:[Product] = []
    @IBOutlet var bagItemsTableView:UITableView!
    @IBOutlet var footerView:UIView!
    @IBOutlet var totalCartPriceLabel: UILabel!
    @IBOutlet var proceedToCheckoutBtn:UIButton!
    @IBOutlet var noItemView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.proceedToCheckoutBtn.layer.borderWidth = 1.0
        self.proceedToCheckoutBtn.layer.borderColor = UIColor.blackColor().CGColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bagItemsTableView.estimatedRowHeight = 88.0
        self.bagItemsTableView.rowHeight = UITableViewAutomaticDimension
        self.reloadBag()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadBag()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController .isKindOfClass(ProductDetailViewController){
            let productDetailViewController = segue.destinationViewController as! ProductDetailViewController
            if segue.identifier == "showProductDetail"{
                let indexPath:NSIndexPath = self.bagItemsTableView.indexPathForSelectedRow!
                productDetailViewController.product = bagItems[indexPath.row]
            }
        }
    }
    
    func reloadBag(){
        self.bagItems = ShoppingBagManager.sharedInstance.allItem()
        
        if self.bagItems.count > 0{
            self.bagItemsTableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
            self.bagItemsTableView.backgroundView = nil
            self.bagItemsTableView.reloadData()
            self.updateItemCountAndPrice()
        }
        else{
            self.bagItemsTableView.separatorStyle = UITableViewCellSeparatorStyle.None
            self.bagItemsTableView.backgroundView = self.noItemView
        }
    }
    
    func updateItemCountAndPrice(){
        var totalCartPrice:Float32 = 0
        var totalItems:Int32 = 0
        
        for product:Product in self.bagItems{
            totalItems += product.itemCount!.integerValue
            if product.price.isDiscountAvailable == true{
                totalCartPrice += (product.price.discountPrice?.floatValue)! * product.itemCount!.floatValue
            }
            else{
                totalCartPrice += product.price.originalPrice.floatValue * product.itemCount!.floatValue
            }
        }
        
        self.totalCartPriceLabel.attributedText = UIUtility.attributedStringForCartSubtotal(totalItems, totalPrice: totalCartPrice)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func didClickProceedToCheckout(sender: UIButton) {
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.bagItems.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:ShoppingBagCell = tableView.dequeueReusableCellWithIdentifier("shoppingBagCell") as! ShoppingBagCell
        cell.didChangeItemCount = {[unowned self](count)->Void in
            let product = self.bagItems[indexPath.row] as Product
            product.itemCount = Int(count)
            self.updateItemCountAndPrice()
        }
        cell.didClickRemoveItem = {[unowned self](sender)->Void in
            let progressHUD:MBProgressHUD = MBProgressHUD()
            progressHUD.label.text = "Shopping Bag"
            progressHUD.detailsLabel.text = "Adding item to shopping bag , please wait .."
            progressHUD.center = self.view.center
            self.view.addSubview(progressHUD)
            progressHUD.showAnimated(true)
            UIUtility.performTask({
                ShoppingBagManager.sharedInstance.removeItemFromShoppingBag(self.bagItems[indexPath.row])
                progressHUD.hideAnimated(true)
                self.updateItemCountAndPrice()
                tableView.beginUpdates()
                self.bagItems.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
                tableView.endUpdates()
                
                if self.bagItems.count == 0{
                    self.reloadBag()
                }
                }, afterDelay: 2.0)
        }
        cell.updateContentOfCell(self.bagItems[indexPath.row])
        return cell
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return self.footerView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.bagItems.count > 0{
            return self.footerView.frame.size.height
        }
        else{
            return 0.0
        }
    }
}
