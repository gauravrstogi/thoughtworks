//
//  ShoppingBagCell.swift
//  ThoughtWorks
//
//  Created by Gaurav Rastogi on 13/10/16.
//  Copyright © 2016 Gaurav Rastogi. All rights reserved.
//

import UIKit
import SDWebImage

class ShoppingBagCell: UITableViewCell {

    @IBOutlet var itemImageView:UIImageView!
    @IBOutlet var itemNameLabel:UILabel!
    @IBOutlet var itemPriceLabel:UILabel!
    @IBOutlet var itemsCountLabel:UILabel!
    @IBOutlet var itemCountStepper: UIStepper!
    
    var didClickRemoveItem:((sender:UIButton)->Void)?
    var didChangeItemCount:((updatedCount:Int)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didChangeNumberOfItems(sender: UIStepper) {
        if let closure = self.didChangeItemCount{
            closure(updatedCount: Int(sender.value))
        }
        
        let itemCount = Int(sender.value)
        itemsCountLabel.text = String(itemCount) + ((itemCount < 2) ? " item" : " items")
    }
    
    @IBAction func didClickRemoveItemFromBag(sender: UIButton) {
        if let closure = self.didClickRemoveItem{
            closure(sender: sender)
        }
    }
    
    func updateContentOfCell(product:Product){
        self.itemCountStepper.value = product.itemCount!.doubleValue
        if let index = product.images.indexOf({ (object:Image) -> Bool in
            return object.isDefault == true
        }){
            itemImageView.sd_setImageWithURL(product.images[index].url, placeholderImage: UIImage(named: "placeholder-small"), options: SDWebImageOptions.LowPriority)
        }
        else{
            itemImageView.image = UIImage(named: "placeholder-small")
        }
        
        itemNameLabel.text = product.name
        
        var finalPrice:NSNumber
        if product.price.isDiscountAvailable == true{
            finalPrice = (product.price.discountPrice?.integerValue)!
        }
        else{
            finalPrice = product.price.originalPrice
        }
        
        itemPriceLabel.text = currency + " " + String(finalPrice.integerValue)
        let intCount:Int = product.itemCount!.integerValue
        itemsCountLabel.text = String(intCount) + ((intCount < 2) ? " item" : " items")
    }

}
